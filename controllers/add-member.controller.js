const fs = require('fs');

exports.addMemberPage = (req, res) => {
    res.render('add-member.ejs', {
        title: "Welcome to Điện Tử Shop | Thêm Thành Viên Mới",
        message: ''
    });
};

exports.addMember = (req, res) => {
    // if (!req.files) {
    //     return res.status(400).send("No files were uploaded.");
    // }

    let message = '';
    let mamb = req.body.mamb;
    let ho = req.body.ho;
    let ten = req.body.ten;
    let email = req.body.email;
    let username = req.body.username;
    let password = req.body.password;
    let phone = req.body.phone;
    let address = req.body.address;
    // let uploadedFile = req.files.avt;
    // let image_name = uploadedFile.name;
    // let fileExtension = uploadedFile.mimetype.split('/')[1];
    // image_name = user + '.' + fileExtension;

    let usernameQuery = "SELECT * FROM `user` WHERE username = '" + username + "'";

    db.query(usernameQuery, (err, result) => {
        if (err) {
            console.log(err);
            return res.status(500).send(err);
        }

        if (result.length > 0) {
            message = 'User Đã Tồn Tại';
            res.render('add-member.ejs', {
                message,
                title: "Welcome to Điện Tử Shop | Thêm Thành Viên Mới"
            });
        } else {
            let query = "INSERT INTO `user` (mamb, ho, ten, email, username, password, phone, address) VALUES ('" +
                        mamb + "', '" + ho + "','" + ten + "', '" + email + "', '" + username + "', '" + password + "', '" + phone + "', '" + address + "')";
                    db.query(query, (err, result) => {
                        if (err) {
                            return res.status(500).send(err);
                        }
                        res.redirect('/homemb');
                    });
            // check the filetype before uploading it
            // if (uploadedFile.mimetype === 'image/png' || uploadedFile.mimetype === 'image/jpeg' || uploadedFile.mimetype === 'image/gif') {
            //     // upload the file to the /public/assets/img directory
            //     uploadedFile.mv(`public/assets/img/${image_name}`, (err ) => {
            //         if (err) {
            //             return res.status(500).send(err);
            //         }
            //         // send the player's details to the database
            //         let query = "INSERT INTO `customer` (ho_ten, dia_chi, dien_thoai, user, pass, avt) VALUES ('" +
            //             ho_ten + "', '" + dia_chi + "','" + dien_thoai + "', '" + user + "', '" + pass + "', '" + image_name + "')";
            //         db.query(query, (err, result) => {
            //             if (err) {
            //                 return res.status(500).send(err);
            //             }
            //             res.redirect('/homemb');
            //         });
            //     });
            // } else {
            //     message = "Invalid File format. Only 'gif', 'jpeg' and 'png' images are allowed.";
            //     res.render('add-member.ejs', {
            //         message,
            //         title: "Welcome to Điện Tử Shop | Thêm Thành Viên Mới"
            //     });
            // }
        }
    });
}

exports.editMemberPage = (req, res) => {
    let playerId = req.params.mamb;
    let query = "SELECT * FROM `user` WHERE mamb = '" + playerId + "' ";
    db.query(query, (err, result) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.render('edit-member.ejs', {
            title: "Chỉnh Sửa Thành Viên",
            player: result[0],
            message: ''
        });
    });
}

exports.editMember = (req, res) => {
    let playerId = req.params.mamb;
    let email = req.body.email;
    let password = req.body.password;
    let address = req.body.address;
    let phone = req.body.phone;

    let query = "UPDATE `user` SET `email` = '" + email + "', `password` = '" + password + "', `address` = '" + address + "', `phone` = '" + phone + "' WHERE `user`.`mamb` = '" + playerId + "'";
    db.query(query, (err, result) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.redirect('/homemb');
    });
}

exports.deleteMember = (req, res) => {
    let playerId = req.params.mamb;
    // let getImageQuery = 'SELECT avt from `customer` WHERE mamb = "' + playerId + '"';
    let deleteUserQuery = 'DELETE FROM user WHERE mamb = "' + playerId + '"';

    db.query(deleteUserQuery, (err, result) => {
        console.log(playerId);
        if (err) {
            return res.status(500).send(err);
        }
        res.redirect('/homemb');
    });
}
