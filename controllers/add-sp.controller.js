const fs = require('fs');

exports.addPlayerPage = (req, res) => {
    res.render('add-sp.ejs', {
        title: "Welcome to Điện Tử Shop | Thêm Sản Phẩm Mới",
        message: ''
    });
    return res;
};

exports.addPlayer = (req, res) => {
    if (!req.files) {
        return res.status(400).send("No files were uploaded.");
    }

    let message = '';
    let masp = req.body.masp;
    let nameProduct = req.body.nameProduct;
    let amountProduct = req.body.amountProduct;
    let priceProduct = req.body.priceProduct;
    let desProduct = req.body.desProduct;
    let idCat = req.body.idCat;
    let showHide = req.body.showHide;
    let uploadedFile = req.files.imgProduct;
    let image_name = uploadedFile.name;
    let fileExtension = uploadedFile.mimetype.split('/')[1];
    image_name = nameProduct + '.' + fileExtension;

    let usernameQuery = "SELECT * FROM `product` WHERE nameProduct = '" + nameProduct + "'";

    db.query(usernameQuery, (err, result) => {
        if (err) {
            console.log(err);
            return res.status(500).send(err);
        }

        if (result.length > 0) {
            message = 'Sản Phẩm Đã Tồn Tại';
            res.render('add-player.ejs', {
                message,
                title: "Welcome to Điện Tử Shop | Thêm Sản Phẩm Mới"
            });
            return res;
        } else {
            // check the filetype before uploading it
            if (uploadedFile.mimetype === 'image/png' || uploadedFile.mimetype === 'image/jpeg' || uploadedFile.mimetype === 'image/gif') {
                // upload the file to the /public/assets/img directory
                uploadedFile.mv(`public/assets/img/${image_name}`, (err ) => {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    // send the player's details to the database
                    let query = "INSERT INTO `product` (masp,nameProduct, amountProduct, imgProduct, priceProduct, desProduct, idCat, showHide) VALUES ('" +
                        masp + "', '" + nameProduct + "', '" + amountProduct + "', '" + image_name + "', '" + priceProduct + "', '" + desProduct + "', '" + idCat + "', '" + showHide + "')";
                    db.query(query, (err, result) => {
                        if (err) {
                            return res.status(500).send(err);
                        }
                        res.redirect('/');
                    });
                });
            } else {
                message = "Invalid File format. Only 'gif', 'jpeg' and 'png' images are allowed.";
                res.render('add-sp.ejs', {
                    message,
                    title: "Welcome to Điện Tử Shop | Thêm Sản Phẩm Mới"
                });
            }
        }
    });
}

exports.editPlayerPage = (req, res) => {
    let playerId = req.params.masp;
    let query = "SELECT * FROM `product` WHERE masp = '" + playerId + "' ";
    db.query(query, (err, result) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.render('edit-sp.ejs', {
            title: "Chỉnh Sửa Sản Phẩm",
            player: result[0],
            message: ''
        });
    });
}

exports.editPlayer = (req, res) => {
    let playerId = req.params.masp;
    let nameProduct = req.body.nameProduct;
    let amountProduct = req.body.amountProduct;
    let priceProduct = req.body.priceProduct;

    let query = "UPDATE `product` SET `nameProduct` = '" + nameProduct + "', `amountProduct` = '" + amountProduct + "', `priceProduct` = '" + priceProduct + "' WHERE `product`.`masp` = '" + playerId + "'";
    db.query(query, (err, result) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.redirect('/');
    });
}

exports.deletePlayer = (req, res) => {
    let playerId = req.params.masp;
    // let getImageQuery = 'SELECT image from `sanpham` WHERE masp = "' + playerId + '"';
    let deleteUserQuery = 'DELETE FROM product WHERE masp = "' + playerId + '"';
    db.query(deleteUserQuery, (err, result) => {
        if (err) {
            return res.status(500).send(err);
        }
        res.redirect('/');
    });
    // db.query(getImageQuery, (err, result) => {
    //     if (err) {
    //         return res.status(500).send(err);
    //     }

    //     let image = result[0].image;

    //     fs.unlink(`public/assets/img/${image}`, (err) => {
    //         if (err) {
    //             return res.status(500).send(err);
    //         }
    //         db.query(deleteUserQuery, (err, result) => {
    //             if (err) {
    //                 return res.status(500).send(err);
    //             }
    //             res.redirect('/');
    //         });
    //     });
    // });
}
