-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 02, 2021 at 05:29 AM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fullface_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `catalog`
--

CREATE TABLE `catalog` (
  `idCat` int(11) NOT NULL,
  `nameCat` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `catalog`
--

INSERT INTO `catalog` (`idCat`, `nameCat`) VALUES
(1, 'Acer'),
(2, 'Asus'),
(3, 'Alienware'),
(4, 'Dell'),
(5, 'Lenovo'),
(6, 'Msi'),
(7, 'HP'),
(8, 'Apple');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE `comment` (
  `idComment` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `ten` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `date` datetime NOT NULL,
  `rating` int(11) NOT NULL,
  `masp` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `comment`
--

INSERT INTO `comment` (`idComment`, `content`, `ten`, `email`, `date`, `rating`, `masp`) VALUES
(1, 'Laptop cùi , lướt facebook 90 chục độ', 'quoc huy', 'kenbi.njr@gmail.com', '2021-03-01 20:06:37', 5, 1),
(2, 'Chơi game cả ngày mà máy có 100 độ', 'Trần Quốc Huy', 'huytqps11190@fpt.edu.vn', '2021-03-01 23:03:11', 3, 2),
(3, 'Máy dùng ổn định', 'E trung Đắk Lắk', 'kenbi.njr@gmail.cm', '2021-03-01 22:32:55', 5, 4),
(4, 'Rẻ nhất so với thị trường', 'Lê Gia Huy', 'lgh@gmail.com', '2021-03-01 22:41:28', 1, 3),
(5, 'Web đẹp quá anh ơi! cho e xin source', 'Quốc Huy', 'kenbi.njr@gmail.cm', '2021-03-01 22:43:02', 5, 2),
(6, 'Tuyển người yêu', 'Biker', 'biker@gmail.com', '2021-03-01 23:34:15', 5, 28),
(7, 'Máy xịn quá', 'Biker', 'biker@gmail.com', '2021-03-01 23:34:54', 1, 25);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `masp` int(11) NOT NULL,
  `nameProduct` varchar(50) DEFAULT NULL,
  `amountProduct` int(11) DEFAULT NULL,
  `imgProduct` varchar(250) DEFAULT NULL,
  `priceProduct` double DEFAULT NULL,
  `dateUpdate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `desProduct` varchar(4000) DEFAULT NULL,
  `idCat` int(11) DEFAULT NULL,
  `showHide` tinyint(1) DEFAULT NULL,
  `views` int(11) DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`masp`, `nameProduct`, `amountProduct`, `imgProduct`, `priceProduct`, `dateUpdate`, `desProduct`, `idCat`, `showHide`, `views`) VALUES
(1, 'Acer Nitro 5', 10, 'img/acer-nitro5.jpg', 20000000, '2021-02-26 04:30:13', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng', 1, 1, 100),
(2, 'Acer Aspire 3', 10, 'img/acer-aspire3.jpg', 17000000, '2021-02-26 04:30:24', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng', 1, 1, 5),
(3, 'Asus TUF A15', 10, 'img/asus-tuf15.jpg', 21000000, '2021-02-26 04:30:59', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng.', 2, 1, 50),
(4, 'Alienware M14', 5, 'img/alienware-m14.jpg', 2000000, '2021-02-26 04:31:47', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng', 3, 1, 0),
(5, 'Alienware M15', 2, 'img/alienware-m15.jpg', 1400000, '2021-02-26 04:31:18', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng', 3, 1, 0),
(6, 'Alienware R15', 3, 'img/alienware-r15.jpg', 4600000, '2021-02-26 04:31:26', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng.', 3, 1, 0),
(7, 'Alienware R17', 5, 'img/dell-r17.jpg', 800000, '2021-02-26 04:31:39', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng.', 3, 1, 0),
(8, 'Dell Inspiron 3593', 6, 'img/dell-inspiron3593.jpg', 2600000, '2021-02-26 04:31:33', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng.', 4, 1, 0),
(17, 'Dell G5', 10, 'img/dell-g5.jpg', 8000000, '2021-03-01 16:12:29', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng.', 4, 1, 7),
(19, 'Lenovo Legion 5P', 4, 'img/legion5p.png', 7000000, '2021-03-01 16:12:05', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng.', 5, 1, 2),
(21, 'Lenovo Idea', 3, 'img/lenovo-idea.jpg', 8000000, '2021-03-01 16:14:04', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng', 5, 1, 8),
(23, 'Lenovo Thinkpad', 4, 'img/lenovo-thinkpad.jpg', 7000000, '2021-03-01 16:15:35', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng.', 5, 1, 2),
(25, 'Msi Modern', 3, 'img/msi-modern.jpg', 16500000, '2021-03-01 16:19:01', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng', 6, 1, 22),
(26, 'Msi GF63', 7, 'img/msi-gf63.jpg', 14850000, '2021-03-01 16:20:45', 'Trang bị cấu hình khủng , dễ dàng cân các tác vụ bình thường đến làm đồ họa khủng', 6, 1, 100);
-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `mamb` int(11) NOT NULL,
  `ho` varchar(50) NOT NULL,
  `ten` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` double DEFAULT NULL,
  `address` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`mamb`, `ho`, `ten`, `email`, `username`, `password`, `phone`, `address`) VALUES
(1, 'admin', 'admin', 'vippro@gmail.cm', 'admin', '123', 932393401, 'VN');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`idCat`);

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`idComment`),
  ADD KEY `masp` (`masp`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`masp`),
  ADD KEY `idCat` (`idCat`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`mamb`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `catalog`
--
ALTER TABLE `catalog`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `comment`
--
ALTER TABLE `comment`
  MODIFY `idComment` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `masp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `mamb` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;



CREATE TABLE IF NOT EXISTS `member` (
    `mamb` int(5) NOT NULL AUTO_INCREMENT,
    `ho_ten` varchar(255) NOT NULL,
    `dia_chi` varchar(255) NOT NULL,
    `vi_tri` varchar(255) NOT NULL,
    `dien_thoai` int(15) NOT NULL,
    `user` varchar(30) NOT NULL,
    `pass` varchar(20) NOT NULL,
    `avt` varchar(255) NOT NULL,
    PRIMARY KEY (`mamb`)
)ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


INSERT INTO `member` (`mamb`, `ho_ten`, `dia_chi`, `vi_tri`, `dien_thoai`, `user`, `pass`) VALUES
(1, 'Nguyễn Văn Toản', 'Huế', 'Quản Trị Viên', '0869354785', 'admin', 'admin');
